=====
Board
=====

.. contents::
   :local:
   :backlinks: none

Libre Space Foundation Board is the governing body of LSF.
As the highest leadership body of the Foundation and to satisfy its fiduciary duties, the board is responsible for:

- determining the mission and purposes of the Foundation
- selecting and evaluating the performance of the chief executive
- strategic and organizational planning
- ensuring strong fiduciary oversight and financial management
- fundraising and resource development
- approving and monitoring the Foundation's programs and services
- enhancing the Foundation's public image
- assessing its performance as the governing body of the Foundation

Joining
^^^^^^^

A new member joining the board requires an existing board member nomination and a unanimous decision among the current board members.