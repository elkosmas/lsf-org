==================
Projects Portfolio
==================

.. contents::
   :local:
   :backlinks: none

.. _Pierros Papadeas: https://gitlab.com/pierros
.. _Vasilis Tsiligiannis: https://gitlab.com/acinonyx
.. _Manolis Surligas: https://gitlab.com/surligas
.. _Agis Zisimatos: https://gitlab.com/zisi
.. _Dimitris Papadeas: https://gitlab.com/dpa2deas
.. _Eleftherios Kosmas: https://gitlab.com/elkosmas
.. _Manthos Papamatthaiou: https://gitlab.com/papamat
.. _Corey Shields: https://gitlab.com/cshields
.. _Xabi Crespo: https://gitlab.com/crespum


Overview
--------

Libre Space Foundation develops and supports a variety of open-source projects for space.
For each project there is a project manager and a LSF Board Champion (more on those [Roles here](Roles)).


Currently active projects
-------------------------

The following major projects are in active development:

+-------------------------------+--------------------------+--------------------------+
| Project                       | Project Manager          | Board Champion           |
+===============================+==========================+==========================+
| `SatNOGS Client and Network`_ | `Vasilis Tsiligiannis`_  | `Pierros Papadeas`_      |
+-------------------------------+--------------------------+--------------------------+
| `SatNOGS DB and DW`_          | `Corey Shields`_         | `Vasilis Tsiligiannis`_  |
+-------------------------------+--------------------------+--------------------------+
| `SatNOGS Radio and Comms`_    | `Manolis Surligas`_      | `Pierros Papadeas`_      |
+-------------------------------+--------------------------+--------------------------+
| `SatNOGS GS Hardware`_        | N/A                      | `Dimitris Papadeas`_     |
+-------------------------------+--------------------------+--------------------------+
| SatNOGS Operations            | `Dimitris Papadeas`_     | `Eleftherios Kosmas`_    |
+-------------------------------+--------------------------+--------------------------+
| PQ9ISH_                       | `Pierros Papadeas`_      | N/A                      |
+-------------------------------+--------------------------+--------------------------+
| Rocketry & Balloons           | `Manthos Papamatthaiou`_ | N/A                      |
+-------------------------------+--------------------------+--------------------------+
| Infrastructure                | `Vasilis Tsiligiannis`_  | `Manthos Papamatthaiou`_ |
+-------------------------------+--------------------------+--------------------------+
| `Polaris`_                    | `Xabi Crespo`_           | `Eleftherios Kosmas`_    |
+-------------------------------+--------------------------+--------------------------+

.. _SatNOGS Client and Network: https://gitlab.com/groups/librespacefoundation/satnogs/_client-and-network/-/shared
.. _SatNOGS DB and DW: https://gitlab.com/groups/librespacefoundation/satnogs/_db-and-dw/-/shared
.. _SatNOGS Radio and Comms: https://gitlab.com/groups/librespacefoundation/satnogs/_radio-and-comms/-/shared
.. _SatNOGS GS Hardware: https://gitlab.com/groups/librespacefoundation/satnogs/_gs-hardware/-/shared
.. _PQ9ISH: https://gitlab.com/librespacefoundation/pq9ish
.. _Polaris: https://gitlab.com/librespacefoundation/polaris


Previously active projects
--------------------------

The following major projects have previously been in active development:

+---------+---------------------+----------------+
| Project | Project Manager     | Board Champion |
+=========+=====================+================+
| UPSat_  | `Pierros Papadeas`_ | N/A            |
+---------+---------------------+----------------+

.. _UPSat: https://gitlab.com/groups/librespacefoundation/satnogs/_client-and-network/-/shared


Contributors
------------

Libre Space Foundation is an active and welcoming community around open source space projects.
We welcome all contributors in our projects and repositories.
We recognize those contributors that have been making considerable contributions in our projects by inviting them in our "LSF Contributors" group.
In practice this recognition (pending LSF board approval) means:

* Access to the `LSF Contributors`_ forum category
* Email alias using the ``user@libre.space`` format
* Recognition in the `About Us`_ page in https://libre.space
* Developer role in the lsf-org_ repository, used for tracking cross-LSF work

Please reach-out to ``pierros@libre.space`` if you think that you should have access to the above, yet you don't seem to.


.. _LSF Contributors: https://community.libre.space/c/lsf-contributors
.. _About Us: https://libre.space/about-us/
.. _lsf-org: https://gitlab.com/librespacefoundation/lsf-org
